
from mpf.core.scriptlet import Scriptlet
from mpf.platforms.diypinball.can_command import ScoreSetCommand, TextSetCommand
from functools import partial

import logging


class Score(Scriptlet):
    def on_load(self):
        self.log = logging.getLogger('Scriptlet (score)')
        self.log.error('Initializing')

        self.diypinball = self.machine.hardware_platforms['diypinball']

        self.machine.events.add_handler('game_started', self.game_started)
        self.machine.events.add_handler('game_ended', self.game_ended)
        self.machine.events.add_handler('ball_started', self.ball_started)
        self.machine.events.add_handler('player_score', self.score_updated)

    def game_started(self, **kwargs):
        self.diypinball.send(ScoreSetCommand(6, 0))
        self.diypinball.send(TextSetCommand(7, 'playball'))

    def game_ended(self, **kwargs):
        self.diypinball.send(ScoreSetCommand(4, 999))
        self.diypinball.send(TextSetCommand(7, 'playover'))

    def ball_started(self, ball, **kwargs):
        self.diypinball.send(ScoreSetCommand(4, ball))

    def score_updated(self, value, **kwargs):
        self.diypinball.send(ScoreSetCommand(6, value))
