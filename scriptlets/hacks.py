
from mpf.core.scriptlet import Scriptlet
from mpf.platforms.diypinball.can_command import SwitchRequestStatusCommand


class Hacks(Scriptlet):
    def on_load(self):
        self.machine.events.add_handler('balldevice_bd_left_kickout_ball_eject_attempt', self.left_kickout_ejected)

    def left_kickout_ejected(self, **kwargs):
        platform = self.machine.hardware_platforms['diypinball']
        switch = self.machine.switches['s_left_kickout'].hw_switch
        switch.state = 0
        #platform.send(SwitchRequestStatusCommand(switch.board, switch.switch))
        #self.machine.switch_controller.process_switch_by_num(state=switch.state,
        #                                                     num=switch.number,
        #                                                     platform=platform)
