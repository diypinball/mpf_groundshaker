
from mpf.core.scriptlet import Scriptlet
from mpf.platforms.diypinball.can_command import ScoreSetCommand
from functools import partial

import logging


class HighScore(Scriptlet):
    def on_load(self):
        self.log = logging.getLogger('Scriptlet (high_score)')
        self.log.error('Initializing')

        self.machine.events.add_handler('mode_attract_starting', self.start)

    def start(self, **kwargs):
        try:
            high_score = int(self.machine.auditor.current_audits['player']['score']['top'][0])
        except IndexError:
            high_score = 0
        finally:
            self.machine.hardware_platforms['diypinball'].send(ScoreSetCommand(5, high_score))

