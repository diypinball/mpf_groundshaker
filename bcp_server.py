#!/usr/bin/env python

import argparse
import socket
import socketserver
import threading

#import mpf.core.bcp.bcp_socket_client as bcp  # 0.31 support
import mpf.core.bcp as bcp
from mpf.platforms.diypinball.can_device import CANDevice
from mpf.platforms.diypinball.can_command import ScoreSetCommand, TextSetCommand


class BCPDecoder(object):
    def __init__(self, *args, **kwargs):
        self.can = CANDevice('can0')
        self.handlers = {
            'config': self.config,
            'mode_start': self.mode_start,
            'mode_stop': self.mode_stop,
            'player_score': self.player_score,
            'trigger': self.trigger
            }

    def process_msg(self, msg):
        try:
            cmd, kwargs = bcp.decode_command_string(msg)
        except ValueError as e:
            print(e)
            return

        if cmd in self.handlers:
            self.handlers[cmd](**kwargs)

    def config(self, **kwargs):
        pass

    def mode_start(self, name, **kwargs):
        if name == 'game':
            self.can.send(ScoreSetCommand(6, 0))
            self.can.send(TextSetCommand(7, 'playball'))

    def mode_stop(self, name, **kwargs):
        if name == 'game':
            self.can.send(ScoreSetCommand(4, 999))
            self.can.send(TextSetCommand(7, 'playover'))

    def player_score(self, value, prev_value, change, player_num, **kwargs):
        self.can.send(ScoreSetCommand(6, value))

    def trigger(self, name, **kwargs):
        if name == 'ball_started':
            self.can.send(ScoreSetCommand(4, kwargs['ball']))


class BCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.decoder = BCPDecoder()
        while True:
            data = self.request.recv(1024).strip()
            if not data:
                break

            self.process_msg(data)

            print("{} wrote:".format(self.client_address[0]))
            print(data)

    def process_msg(self, buf):
        buf = buf.decode('ascii')
        msgs = buf.split('\n')
        for msg in msgs:
            self.decoder.process_msg(msg)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', '-p', type=int, help='port to serve on', default=5050)
    parser.add_argument('--address', '-a', type=str, help='address to serve on', default='localhost')
    args = parser.parse_args()

    server = socketserver.TCPServer((args.address, args.port), BCPHandler)
    server.serve_forever()


if __name__ == '__main__':
    main()

