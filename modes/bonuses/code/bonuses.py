import logging
import time

from functools import partial
from mpf.core.mode import Mode


class LEDDisplay(object):
    def __init__(self, led_names, machine_leds):
        self.names = led_names
        self.leds = machine_leds
        self._value = 0

    def on(self, led_index, brightness=10, r=255, g=255, b=255):
        name = self.names[led_index]
        r = int(r * brightness / 10)
        g = int(g * brightness / 10)
        b = int(b * brightness / 10)
        self.leds[name].hw_driver.color([r, g, b])

    def off(self, led_index):
        name = self.names[led_index]
        self.leds[name].hw_driver.color([0, 0, 0])

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

        if value >= 20:
            self.on(10, g=0, b=0)
            value -= 20
        else:
            self.off(10)
        if value >= 10:
            self.on(9, r=0, b=0)
            value -= 10
        else:
            self.off(9)

        remaining = (value % 10)

        for i in range(remaining - 1):
            self.on(i, 2)
        if remaining > 0:
            self.on(remaining - 1)
        for i in range(remaining, 9):
            self.off(i)


class Bonuses(Mode):
    leds = {
        'left_advance': [
            'd_left_1',
            'd_left_2',
            'd_left_3',
            'd_left_4',
            'd_left_5',
            'd_left_6',
            'd_left_7',
            'd_left_8',
            'd_left_9',
            'd_left_10',
            'd_left_20'
        ],

        'right_advance': [
            'd_right_1',
            'd_right_2',
            'd_right_3',
            'd_right_4',
            'd_right_5',
            'd_right_6',
            'd_right_7',
            'd_right_8',
            'd_right_9',
            'd_right_10',
            'd_right_20'
        ]}

    def mode_start(self, **kwargs):
        self.log = logging.getLogger('Mode Base (bonuses)')
        self.log.info('Initializing')
        self.left_advance = LEDDisplay(self.leds['left_advance'], self.machine.leds)
        self.right_advance = LEDDisplay(self.leds['right_advance'], self.machine.leds)

        self.machine.events.add_handler('ball_ended', self.ball_end)
        self.machine.events.add_handler('player_left_advance', partial(self.display_multiplier, 'left_advance'))
        self.machine.events.add_handler('player_right_advance', partial(self.display_multiplier, 'right_advance'))

    def display_multiplier(self, var, value, **kwargs):
        if value > 39:
            value = 39
            self.machine.game.player.vars[var] = 39

        getattr(self, var).value = value

    def ball_end(self, **kwargs):
        self.machine.game.player.vars['left_advance'] = 0
        self.machine.game.player.vars['right_advance'] = 0

        while self.left_advance.value > 0 or self.right_advance.value > 0:
            add_score = 0
            if self.left_advance.value > 0:
                self.left_advance.value -= 1
                add_score += 1000
            if self.right_advance.value > 0:
                self.right_advance.value -= 1
                add_score += 1000
            self.player['score'] += add_score
            time.sleep(0.1)

