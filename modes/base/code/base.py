import logging

from functools import partial
from mpf.core.mode import Mode


class Base(Mode):
    def mode_start(self, **kwargs):
        self.reset_bonus_lights()
        self.log = logging.getLogger('Mode Base (bonuses)')
        self.log.error('Initializing')
        
        self.machine.events.add_handler('player_bonus_lights_increment', self.display_player_bonus)
        self.machine.events.add_handler('player_droptarget_complete', self.droptarget_bonus)
        self.machine.events.add_handler('player_athruf_complete', self.athruf_bonus)

    def reset_bonus_lights(self):
        self.bonus_lights = [
                'l_lights_left_bonus',
                'l_lights_right_bonus',
            ]

    def display_player_bonus(self, value, **kwargs):

        for light in self.bonus_lights:
            self.machine.lights[light].off()

        self.machine.lights[self.bonus_lights[value % 2]].on()

    def droptarget_bonus(self, value, **kwargs):
        if value == 1:
            #first time the drop targets go down 5000pts, lights extra ball
            self.player['score'] +=  5000
            self.machine.lights['l_extra_ball'].on()
        elif value ==2:
            #second time the drop targets go down award extra ball, light special
            #award extra ball
            #self.player['score'] +=  25000 #placeholder for "extra ball"
            self.machine.events.post('dt_extra_ball_award')
            self.machine.lights['l_shoot_again'].on()
            self.machine.lights['l_special_left'].on()
            self.machine.lights['l_special_right'].on()
            self.machine.lights['l_special'].on()
        else:
            #third and every time after that the drop targets go down, Award replay
            self.player['score'] +=  50000  #placeholder for "free game"
    
    
    def athruf_bonus(self, value, **kwargs):
        if value == 1:
            #first time the A through F lights light 20k bonus
            self.player['score'] +=  20000 #remove this as it is awarded durring bonus
            self.machine.leds['d_20k_super'].color([255, 0, 0])
        elif value ==2:
            #second time A thorough F lights 30k bonus
            self.player['score'] +=  30000 #remove this as it is awarded durring bonus
            self.machine.leds['d_30k_nitro'].color([0, 255, 0])
        else:
            #third and every time after that A through F lights, award replay
            self.player['score'] +=  30000 #placeholder for "replay"
            self.machine.leds['d_center_special'].color([0, 0, 255])

